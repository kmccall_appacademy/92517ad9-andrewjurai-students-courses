class Student
  attr_accessor :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = Array.new
  end

  def name
    [@first_name, @last_name].join(" ")
  end

  def enroll(new_course)
    @courses << new_course unless @courses.include? new_course
    new_course.students << self
  end

  def course_load
    courseload = Hash.new(0)
    self.courses
    self.courses.each { |course| courseload[course.department] += course.credits }
    courseload
  end

end
